const { gql } = require("apollo-server-core");
const { Users, Licenses, Servers } = require("./models");
const crypto = require("crypto");
const moment = require("moment");

exports.typeDefs = gql`
  type Query {
    users: [users!]!
    user: user!
    license: license
    server: server
  }

  type user {
    id: ID!
    email: String!
    class: String!
  }

  type users {
    id: ID!
    email: String!
    class: String!
    license: license
  }

  type license {
    id: ID!
    email: String!
    registered: String!
    expired: String!
    key: String!
    server: server
  }

  type server {
    id: ID!
    key: String!
    email: String!
    domain: String!
    ip: String!
  }

  type Mutation {
    createLicense(id: String!): license!
    createServer(email: String!, ip: String!, domain: String!): server!
    deleteLicense(id: String!): license!
    deleteServer: server!
    updateRole(id: String!, value: String!): user!
  }
`;

exports.resolvers = {
  Query: {
    users: async (_, {}, context) => {
      if (!context) return Error("unauthorize");
      if (context.class != "admin") return Error("unauthorize");
      return await Users.find();
    },
    user: async (_, {}, context) => {
      if (!context) return Error("unauthorize");
      let user = await Users.findOne({ email: context.email });
      return user;
    },
    license: async (_, {}, context) => {
      if (!context) return Error("unauthorize");
      let license = await Licenses.findOne({ email: context.email });
      return license;
    },
    server: async (_, {}, context) => {
      if (!context) return Error("unauthorize");
      let license = await Licenses.findOne({ email: context.email });
      if (!license) return Error("server error");
      let server = await Servers.findOne({ key: license.key });
      return server;
    },
  },

  users: {
    license: async (_, {}, context) => {
      if (!context) return Error("unauthorize");
      if (context.class != "admin") return Error("unauthorize");
      let lic = await Licenses.findOne({ email: _.email });
      return lic;
    },
  },

  license: {
    server: async (_, {}, context) => {
      if (!context) return Error("unauthorize");
      let server = await Servers.findOne({ key: _.key });
      return server;
    },
  },

  Mutation: {
    createLicense: async (_, { id }, context) => {
      if (!context) return Error("unauthorize");
      if (context.class != "admin") return Error("unauthorize");
      let user = await Users.findById(id);
      if (!user) return Error("server error");
      let data = {
        email: user.email,
        key: crypto.randomBytes(32).toString("hex"),
        registered: Date.now(),
        expired: moment().add(30, "days").valueOf(),
      };
      let license = new Licenses(data);
      return await license.save();
    },
    createServer: async (_, { email, ip, domain }, context) => {
      if (!context) return Error("unauthorize");
      let license = await Licenses.findOne({ email: context.email });
      if (!license) return Error("server error");
      let data = {
        key: license.key,
        email,
        ip,
        domain,
      };
      let chk = await Servers.findOne({ key: license.key });
      if (chk) return new Error("Server already been set up before");
      let server = new Servers(data);
      return await server.save();
    },
    deleteLicense: async (_, { id }, context) => {
      if (!context) return Error("unauthorize");
      if (context.class != "admin") return Error("server error");
      let user = await Users.findById(id);
      if (!user) return Error("server error");
      let license = await Licenses.findOne({ email: user.email });
      if (!license) return Error("server error");
      await Servers.findOneAndDelete({ key: license.key });
      return await Licenses.findOneAndDelete({ key: license.key });
    },
    deleteServer: async (_, {}, context) => {
      if (!context) return Error("unauthorize");
      let license = await Licenses.findOne({ email: context.email });
      if (!license) return Error("server error");
      return await Servers.findOneAndDelete({ key: license.key });
    },
    updateRole: async (_, { id, value }, context) => {
      if (!context) return Error("server error");
      if (context.class != "admin") return Error("server error");
      let user = await Users.findByIdAndUpdate(id, { class: value });
      return user;
    },
  },
};
