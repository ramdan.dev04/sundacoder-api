const express = require("express");
const { json, urlencoded } = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const ServerlessHttp = require("serverless-http");
const { ApolloServer } = require("apollo-server-express");
const { typeDefs, resolvers } = require("./schema");
const mongoose = require("mongoose");
const { decodeToken } = require("./helper");
const { Users } = require("./models");
const route = require("./router");
const cors = require("cors");
mongoose.connect(
    "mongodb+srv://Admur04:Admur04012000@admur.vjdia.mongodb.net/saido?retryWrites=true&w=majority",
    (err) => {
        if (err) console.log("cant connect to database");
    }
);

const api = express();

api.use(cors());
api.use(json());
api.use(urlencoded({ extended: false }));
api.use(cookieParser());
api.use(
    session({
        secret: "Admur123",
        saveUninitialized: false,
        resave: false,
    })
);

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: async ({ req }) => {
        let token = req.headers.authorization || "";
        token = token.replace("Bearer ", "");
        let data = await decodeToken(token);
        // console.log(data)
        if (!data) return false;
        let user = await Users.findOne({ email: data.email });
        // console.log(user)
        if (!user) return false;
        // console.log(user)
        return user;
    },
});

server.applyMiddleware({ app: api });

api.use("/", route);

api.use((req, res) => {
    res.json({ code: 500 });
});

exports.handler = ServerlessHttp(api);
