const { Blocker, Servers, Ip } = require("../models");

exports.Auth = async (req, res, next) => {
  var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
  if (ip.match(/::ffff:/i)) {
    ip = ip.replace(/::ffff:/i, "");
  }
  if (ip == "::1") {
    ip = "127.0.0.1";
  }
  let check = await Servers.findOne({ ip });
  if (!check) {
    let st = new Ip({ ip, message: "unauthorized server" });
    await st.save();
    res.status(401);
    res.json({ code: 401 });
    return;
  }
  return next();
};

exports.blockHost = async (req, res, next) => {
  const data = req.body;
  let bh = await Blocker.findOne({ name: "host1" });
  bh = bh.ip;
  let block = false;
  await bh.forEach((ok) => {
    if (data.host.indexOf(ok) > -1) {
      block = true;
    }
  });
  if (block) {
    return res.json({ code: 200, block, by: 0 });
  }
  next();
};

exports.blockIp1 = async (req, res, next) => {
  const data = req.body;
  let bh = await Blocker.findOne({ name: "ip1" });
  bh = bh.ip;
  let block = false;
  if (bh.includes(data.ip1)) {
    block = true;
  } else {
    bh.forEach((ok) => {
      const rgx = new RegExp(ok, "i");
      if (data.ip1.match(rgx)) {
        block = true;
      }
    });
  }
  if (block) {
    return res.json({ code: 200, block, by: 1 });
  }
  return next();
};

exports.blockIp2 = async (req, res, next) => {
  let data = req.body;
  let bh = await Blocker.findOne({ name: "ip2" });
  bh = bh.ip;
  let block = false;
  bh.forEach((ok) => {
    let regex = new RegExp(ok, "i");
    if (data.ip1.match(regex)) {
      block = true;
    }
  });
  if (block) return res.json({ code: 200, block, by: 2 });
  next();
};

exports.blockUa1 = async (req, res, next) => {
  let data = req.body;
  let bh = await Blocker.findOne({ name: "ua1" });
  bh = bh.ip;
  let block = false;
  bh.forEach((ok) => {
    let myua = data.ua.toLowerCase();
    if (
      myua.indexOf(ok.toLowerCase()) > -1 ||
      myua == "" ||
      myua == " " ||
      myua == "  " ||
      myua == "      "
    ) {
      block = true;
    }
  });
  if (block) return res.json({ code: 200, block, by: 3 });
  next();
};

exports.blockUa2 = async (req, res, next) => {
  let data = req.body;
  let bh = await Blocker.findOne({ name: "ua2" });
  bh = bh.ip;
  let block = false;
  bh.forEach((ok) => {
    let myua = data.ua;
    let regex = new RegExp(ok, "i");
    if (myua.match(regex)) {
      block = true;
    }
  });
  if (block) return res.json({ code: 200, block, by: 4 });
  next();
};

exports.blockIsp1 = async (req, res, next) => {
  let data = req.body;
  let bh = await Blocker.findOne({ name: "isp1" });
  bh = bh.ip;
  let block = false;
  bh.forEach((ok) => {
    if (data.isp.indexOf(ok) > -1) {
      block = true;
    }
  });
  if (block) return res.json({ code: 200, block, by: 5 });
  res.json({ code: 200, block });
};
