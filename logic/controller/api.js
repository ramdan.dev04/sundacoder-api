const { decodeToken } = require("../helper");
const { Users, Servers, Licenses } = require("../models");
const dns = require("dns");

exports.Login = async (req, res) => {
    let token = req.headers.authorization || "";
    token = token.replace("Bearer ", "");
    let data = await decodeToken(token);
    if (data) {
        let user = await Users.findOne({ email: data.email });
        if (user) {
            res.json({ code: 200, from: 1 });
        } else {
            let start = new Users({ email: data.email });
            await start.save();
            res.json({ code: 200 });
        }
    } else {
        res.status(500);
        return res.json({ code: 500 });
    }
};

exports.checkDns = (req, res) => {
    let domain = req.body.domain;
    dns.resolve4(domain, (err, address) => {
        if (err) return res.json({ code: 500, message: err.message });
        res.json(address);
    });
};

exports.apiLicense = async (req, res) => {
    var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
    if (ip.match(/::ffff:/i)) {
        ip = ip.replace(/::ffff:/i, "");
    }
    if (ip == "::1") {
        ip = "127.0.0.1";
    }
    let server = await Servers.findOne({ ip });
    if (!server) return res.json({ code: 401, ip });
    let lic = await Licenses.findOne({ key: server.key });
    if (!lic) return res.json({ code: 401, ip });
    res.json({ code: 200, ip: ip });
};
